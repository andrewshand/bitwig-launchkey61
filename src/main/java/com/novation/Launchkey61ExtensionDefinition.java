package com.novation;
import java.util.UUID;

import com.bitwig.extension.api.PlatformType;
import com.bitwig.extension.controller.AutoDetectionMidiPortNamesList;
import com.bitwig.extension.controller.ControllerExtensionDefinition;
import com.bitwig.extension.controller.api.ControllerHost;

public class Launchkey61ExtensionDefinition extends ControllerExtensionDefinition
{
   private static final UUID DRIVER_ID = UUID.fromString("552cffa0-590d-4392-9ceb-8c5fa2a2bb3c");
   
   public Launchkey61ExtensionDefinition()
   {
   }

   @Override
   public String getName()
   {
      return "Launchkey 61";
   }
   
   @Override
   public String getAuthor()
   {
      return "andrewshand94";
   }

   @Override
   public String getVersion()
   {
      return "0.1";
   }

   @Override
   public UUID getId()
   {
      return DRIVER_ID;
   }
   
   @Override
   public String getHardwareVendor()
   {
      return "Novation";
   }
   
   @Override
   public String getHardwareModel()
   {
      return "Launchkey 61";
   }

   @Override
   public int getRequiredAPIVersion()
   {
      return 3;
   }

   @Override
   public int getNumMidiInPorts()
   {
      return 2;
   }

   @Override
   public int getNumMidiOutPorts()
   {
      return 2;
   }

   @Override
   public void listAutoDetectionMidiPortNames(final AutoDetectionMidiPortNamesList list, final PlatformType platformType)
   {
      if (platformType == PlatformType.WINDOWS)
      {
         // TODO: Set the correct names of the ports for auto detection on Windows platform here
         // and uncomment this when port names are correct.
         // list.add(new String[]{"Input Port 0", "Input Port 1"}, new String[]{"Output Port 0", "Output Port -1"});
      }
      else if (platformType == PlatformType.MAC)
      {
         list.add(
            new String[]{"Launchkey MK2 61 Launchkey MIDI", "Launchkey MK2 61 Launchkey InControl"},
            new String[]{"Launchkey MK2 61 Launchkey MIDI", "Launchkey MK2 61 Launchkey InControl"}
         );
      }
      else if (platformType == PlatformType.LINUX)
      {
         // TODO: Set the correct names of the ports for auto detection on Windows platform here
         // and uncomment this when port names are correct.
         // list.add(new String[]{"Input Port 0", "Input Port 1"}, new String[]{"Output Port 0", "Output Port -1"});
      }
   }

   @Override
   public Launchkey61Extension createInstance(final ControllerHost host)
   {
      return new Launchkey61Extension(this, host);
   }
}
