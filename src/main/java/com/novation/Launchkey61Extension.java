package com.novation;

import com.bitwig.extension.api.util.midi.ShortMidiMessage;
import com.bitwig.extension.callback.ShortMidiMessageReceivedCallback;
import com.bitwig.extension.controller.api.*;
import com.bitwig.extension.controller.ControllerExtension;

public class Launchkey61Extension extends ControllerExtension
{
   private MidiOut midiOutPort1;
   private TrackBank trackBankSection;
   private MasterTrack masterTrack;
   private CursorDevice currDevice;
   private CursorTrack cursorTrack;
   private CursorRemoteControlsPage remoteControlsPage;
   private Browser browser;
   boolean masterFaderDoesCurrentTrack = false;
   protected Launchkey61Extension(final Launchkey61ExtensionDefinition definition, final ControllerHost host)
   {
      super(definition, host);
   }

   @Override
   public void init()
   {
      final ControllerHost host = getHost();
      cursorTrack = host.createCursorTrack(0, 0);
      currDevice = cursorTrack.createCursorDevice("primary", "Primary", 0, CursorDeviceFollowMode.FOLLOW_SELECTION);
      browser = currDevice.createDeviceBrowser(8, 8);
      remoteControlsPage = currDevice.createCursorRemoteControlsPage(8);
      remoteControlsPage.pageNames().markInterested();
      remoteControlsPage.selectedPageIndex().markInterested();

      midiOutPort1 = host.getMidiOutPort(1);
      mTransport = host.createTransport();
      trackBankSection = host.createTrackBank(8, 0, 0);
      masterTrack = host.createMasterTrack(1);

      MidiIn midiInPort0 = host.getMidiInPort(0);

      MidiIn midiInPort1 = host.getMidiInPort(1);

      midiInPort0.setMidiCallback((ShortMidiMessageReceivedCallback)msg -> onMidi0(msg));
      midiInPort0.setSysexCallback(this::onSysex0);
      midiInPort1.setMidiCallback((ShortMidiMessageReceivedCallback)msg -> onMidi1(msg));
      midiInPort1.setSysexCallback(this::onSysex1);
      midiInPort0.createNoteInput("Keys", "B0????", "80????", "90????", "B001??", "D0????", "E0????");

      // set the keyboard into InControl mode
      midiOutPort1.sendMidi(159, 12,127);

      // TODO: Perform your driver initialization here.
      // For now just show a popup notification for verification that it is running.
      host.showPopupNotification("Launchkey 61 Initialized");

      updateLights();
   }

   @Override
   public void exit()
   {
      midiOutPort1.sendMidi(159, 12,0);
      // TODO: Perform any cleanup once the driver exits
      // For now just show a popup notification for verification that it is no longer running.
      getHost().showPopupNotification("Launchkey 61 Exited");

   }

   @Override
   public void flush()
   {
      // TODO Send any updates you need here.
   }

   void println(String msg) {
      getHost().println(msg);
   }

   /** Called when we receive short MIDI message on port 0. */
   private void onMidi0(ShortMidiMessage msg) 
   {
//      println("Port 0");
//      println("Channel: " + msg.getChannel());
//      println("Data1:" + msg.getData1());
//      println("Data2:" + msg.getData2());
//      println("Status:" + msg.getStatusByte());
   }

   /** Called when we receive sysex MIDI message on port 0. */
   private void onSysex0(final String data) 
   {
   }

   private void onFaderChange(int index, byte value) {

   }

   private void onKnobChange(int index, byte value) {
      remoteControlsPage.getParameter(index).value().set(value / 127f);
   }

   private int colorForPadIndex(int index) {
      return (new int[] {6, 9, 109, 22, 19, 41, 54, 58})[index % 8];
   }

   private int noteForPad(int intended) {
      if (intended < 8) {
         return intended + 96;
      }
      return intended + 112 - 8;
   }

   private void onPadButtonChange(int index, boolean on) {
      if (!on) {
         return;
      }

      int count = remoteControlsPage.pageNames().get().length;
      if (index >= count) {
         return;
      }

      for (int i = 0; i < remoteControlsPage.getParameterCount(); i++) {
         remoteControlsPage.getParameter(i).setIndication(false);
      }
      remoteControlsPage.selectedPageIndex().set(index);
      getHost().showPopupNotification(remoteControlsPage.pageNames().get(index));

      for (int i = 0; i < remoteControlsPage.getParameterCount(); i++) {
         remoteControlsPage.getParameter(i).setIndication(true);
      }

      updateLights();
   }

   private void updateLights() {
      int pageCount = remoteControlsPage.pageNames().get().length;
      int selectedIndex = remoteControlsPage.selectedPageIndex().get();
      for (int i = 0; i < 17; i++) {
         if (i == 8) {
            continue; // skip the irrelevant button
         }
         int correctedIndex = i < 8 ? i : i - 1; // the extra button has pushed us up one
         int note = noteForPad(correctedIndex);
         int color = colorForPadIndex(correctedIndex);
         midiOutPort1.sendMidi(0x9F, note, color);
         if (selectedIndex == correctedIndex) {
            midiOutPort1.sendMidi(0x91, note, 1);
         }
      }
      midiOutPort1.sendMidi(191, 59, masterFaderDoesCurrentTrack ? 127 : 0);

   }

   private void onPadSecondaryButtonChange(int index, boolean on) {
      if (!on) {
         return;
      }
   }

   private void onFaderButtonChange(int index, boolean on) {

   }

   private void onMasterFaderChange(byte value) {
      if (masterFaderDoesCurrentTrack) {
         cursorTrack.getVolume().set(value / 127f);
      }
      else {
         masterTrack.getVolume().set(value / 127f);
      }
   }

   private void onMasterButtonChange(boolean on) {
      if (!on) {
         return;
      }

      masterFaderDoesCurrentTrack = !masterFaderDoesCurrentTrack;
      updateLights();
   }

   /** Called when we receive short MIDI message on port 1. */
   private void onMidi1(ShortMidiMessage msg) 
   {
//      println("Port 1");
//      println("Channel: " + msg.getChannel());
//      println("Data1:" + msg.getData1());
//      println("Data2:" + msg.getData2());
//      println("Status:" + msg.getStatusByte());

      if (msg.getStatusByte() == 159 && msg.getData2() == 0) {
         // in control change, force them back into control
         //midiOutPort1.sendMidi(159, 14, 127); // sliders
         //midiOutPort1.sendMidi(159, 13, 127); // pots
         //midiOutPort1.sendMidi(159, 15, 127); // drum pads
      }
      if (msg.isNoteOn() || msg.isNoteOff()) {
         if (msg.getData1() >= 96 && msg.getData1() <= 103) {
            // first row of pads
            onPadButtonChange(msg.getData1() - 96, msg.isNoteOn());
         }
         if (msg.getData1() >= 112 && msg.getData1() <= 119) {
            // second row of pads
            onPadButtonChange(msg.getData1() - 112 + 8, msg.isNoteOn());
         }
         if (msg.getData1() == 104 || msg.getData1() == 120) {
            // buttons next to pads
            onPadSecondaryButtonChange(msg.getData1() == 104 ? 0 : 1, msg.isNoteOn());
         }
      }
      if (msg.isControlChange()) {
         if (msg.getData1() >= 21 && msg.getData1() <= 28) {
            onKnobChange(msg.getData1() - 21, (byte) msg.getData2());
         }
         if (msg.getData1() >= 41 && msg.getData1() <= 48) {
            onFaderChange(msg.getData1() - 41, (byte) msg.getData2());
         }
         if (msg.getData1() >= 51 && msg.getData1() <= 58) {
            onFaderButtonChange(msg.getData1() - 51, msg.getData2() == 127);
         }
         if (msg.getData1() == 7) {
            onMasterFaderChange((byte) msg.getData2());
         }

         if (msg.getData2() == 127) {
            // 127 == key down only
            if (msg.getData1() == 59) {
               onMasterButtonChange(msg.getData2() == 127);
            }
            if (msg.getData1() == 112) {
               mTransport.rewind();
            }
            if (msg.getData1() == 113) {
               mTransport.fastForward();
            }
            if (msg.getData1() == 114) {
               mTransport.stop();
            }
            if (msg.getData1() == 115) {
               mTransport.play();
            }
            if (msg.getData1() == 116) {
               mTransport.isArrangerLoopEnabled().toggle();
            }
            if (msg.getData1() == 117) {
               mTransport.isArrangerRecordEnabled().toggle();
            }
            if (msg.getData1() == 102) {
               cursorTrack.selectPrevious();
            }
            else if (msg.getData1() == 103) {
               cursorTrack.selectNext();
            }
         }

      }
      // TODO: Implement your MIDI input handling code here.
   }

   /** Called when we receive sysex MIDI message on port 1. */
   private void onSysex1(final String data) 
   {
   }

   private Transport mTransport;
}
