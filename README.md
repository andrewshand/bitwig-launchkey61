Bitwig extension written in Java specificaly for Launchkey 61

- [x] Pads
- [x] Knobs
- [ ] Faders
- [x] Keys
- [x] Sustain pedal
- [x] Pitch wheel/mod
- [x] Transport (play, record etc.)

###### Pads & Knobs
The pads switch between pages of device controls from left to right, and the knobs control the parameters on that page. I've colour coded each pad to match the colours that Bitwig gives each parameters. While this loses the indication of how many pages of parameters are available, I find it much faster to actually find the parameter on screen I want to change.

###### Faders
The faders don't do anythnig now, except for the master fader, which controls either the master fader or the current track volume (if the button underneath is pressed).

Is it even practical to use these faders to control track volumes? Trying to map a set of 8 tracks from the screen to my hands on the keyboard does not seem intuitive.
